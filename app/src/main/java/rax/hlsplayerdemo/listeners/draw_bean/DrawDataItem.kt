package rax.hlsplayerdemo.listeners.draw_bean

data class DrawDataItem(
	val date: String? = null,
	val updatedOn: String? = null,
	val id: String? = null,
	val time: String? = null,
	val draw: String? = null,
	val fullDate: String? = null,
	val addedOn: String? = null,
	val status: String? = null
)
