package rax.hlsplayerdemo.listeners.draw_bean

data class ErrorModel(
	val status: String? = null,
	val message: String? = null
)
