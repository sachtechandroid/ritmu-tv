package rax.hlsplayerdemo.listeners.draw_bean

data class DrawResponse(
	val status: String? = null,
	val drawData: List<DrawDataItem?>? = null,
	val message: String? = null
)
