package rax.hlsplayerdemo.listeners;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Balwinder Badgal on 12/14/2017.
 */
public class Api_Hitter {
    public static Retrofit retrofit = null;


    public static Api_Hitter.AApiInterface ApiInterface() {

        retrofit = new Retrofit.Builder()
                .baseUrl("https://ritmu.tv/Ritmu/admin/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api_Hitter.AApiInterface apiInterface = retrofit.create(Api_Hitter.AApiInterface.class);
        return apiInterface;

    }

    public interface AApiInterface {

        @POST("api/drawApi.php")
        @FormUrlEncoded
        Call<JsonObject> getDrawList(
                @Field("date") String date,
                @Field("type") String type);

    }

}
