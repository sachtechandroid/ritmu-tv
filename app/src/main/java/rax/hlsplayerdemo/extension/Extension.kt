package rax.hlsplayerdemo.extension

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.Toast
import rax.hlsplayerdemo.app.App

fun toast(text: String) {
    Toast.makeText(App.application, text, Toast.LENGTH_LONG).show()
}

fun Context.isPermissionsGranted(p0: Array<String>): Boolean {
    p0.forEach {
        if (ContextCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_DENIED)
            return false
    }
    return true
}


fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}


fun isInternet(): Boolean {
    val isInternet: Boolean

    val manager = App.application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val info = manager.activeNetworkInfo
    return info != null && info.isConnectedOrConnecting

}

fun askForPermissions(activity: Activity, array: Array<String>) {
    ActivityCompat.requestPermissions(activity, array, 0)
}

inline fun <reified T> Context.openActivity() {
    startActivity(Intent(this, T::class.java))
}

inline fun <reified T> Context.openActivity(key: String, extra: String) {
    var intent = Intent(this, T::class.java)
    intent.putExtra(key, extra)
    startActivity(intent)
}


inline fun Context.delay(timeMS: Long, crossinline body: () -> Unit): Handler {
    return Handler().apply {
        postDelayed({
            body()
        }, timeMS)
    }
}

inline fun Any.workerRepeatRH(timeMS: Long, crossinline body: () -> Unit): Handler {
    val handler = Handler()
    var runnable: Runnable? = null
    runnable = Runnable {
        body()
        handler.postDelayed(runnable, timeMS)
    }
    handler.postDelayed(runnable, timeMS)
    return handler
}

inline fun executeSafe(body: () -> Unit) {
    try {
        body.invoke()
    } catch (e: Exception) {

    }
}
