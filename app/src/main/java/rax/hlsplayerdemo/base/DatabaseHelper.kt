package rax.hlsplayerdemo.base

import android.service.autofill.UserData
import com.google.firebase.database.*


class DatabaseHelper {
    private val user = "User"
    private val database by lazy { FirebaseDatabase.getInstance() }

    fun getUser(id: String): DatabaseReference {
        val myRef = getUserdatabaseReference().child(id)
        return myRef
    }

    fun createUser(id: String, userData: UserData) {
        getUserdatabaseReference().child(id).setValue(userData)
    }

    fun checkUser(id: String): Boolean {
        var exist = false
        val myRef = getUserdatabaseReference().child(id).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {


            }

            override fun onDataChange(p0: DataSnapshot) {
                exist = p0.exists()
            }
        })

        return exist
    }

    fun getUserdatabaseReference() = database.getReference(user)
}


