package rax.hlsplayerdemo.base

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.graphics.Color
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import java.util.regex.Pattern

class Utils {
    var green = "#96cc45"
    val utils by lazy { Utils() }
    fun setLeadingZero(no: Int): String {
        var str = "" + no
        if (no < 10)
            str = "0$no"
        return str.trim { it <= ' ' }
    }

    fun showToastS(text: String, context: Context) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
    }

    fun setupUI(view: View, activity: Activity) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (view !is EditText) {

            view.setOnTouchListener { v, event ->
                // TODO Auto-generated method stub
                hideSoftKeyboard(activity)
                false
            }
        }

    }

    fun createSimpleDialog1(context: Context, title: String,
                            msg: String, btnLabel1: String, method1: Method?): Dialog {
        val str = SpannableString(title)
        str.setSpan(ForegroundColorSpan(Color.parseColor(green)), 0,
                title.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        val builder = AlertDialog.Builder(context)
        builder.setTitle(str)
        builder.setMessage(msg)

        builder.setPositiveButton(btnLabel1
        ) { dialog, which ->
            if (method1 != null)
                method1!!.execute()
        }

        val d = builder.show()
        val dividerId = d.context.resources
                .getIdentifier("android:id/titleDivider", null, null)
        val divider = d.findViewById<View>(dividerId)
        divider.setBackgroundColor(Color.parseColor(green))

        return d

    }


    fun createSimpleDialog(context: Context, title: String,
                           msg: String, btnLabel1: String, btnLabel2: String, method1: Method?): Dialog {
        val str = SpannableString(title)
        str.setSpan(ForegroundColorSpan(Color.parseColor(green)), 0,
                title.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        val builder = AlertDialog.Builder(context)
        builder.setTitle(str)
        builder.setMessage(msg)

        builder.setPositiveButton(btnLabel1,
                object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface, which: Int) {

                        if (method1 != null)
                            method1!!.execute()
                    }
                })

        builder.setNegativeButton(btnLabel2,
                object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface, which: Int) {

                        // if (method2 != null)
                        // method2.execute();
                    }
                })
        val d = builder.show()
        val dividerId = d.context.resources
                .getIdentifier("android:id/titleDivider", null, null)
        val divider = d.findViewById<View>(dividerId)
        divider.setBackgroundColor(Color.parseColor(green))

        return d

    }


    fun createSimpleDialogone(context: Context, title: String,
                              msg: String, btnLabel1: String): Dialog {
        val str = SpannableString(title)
        str.setSpan(ForegroundColorSpan(Color.parseColor(green)), 0,
                title.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        val builder = AlertDialog.Builder(context)
        builder.setTitle(str)
        builder.setMessage(msg)

        builder.setNegativeButton(btnLabel1,
                object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface, which: Int) {

                        // if (method2 != null)
                        // method2.execute();
                    }
                })
        val d = builder.show()
        val dividerId = d.context.resources
                .getIdentifier("android:id/titleDivider", null, null)
        val divider = d.findViewById<View>(dividerId)
        divider.setBackgroundColor(Color.parseColor(green))

        return d

    }

    fun createSimpleDialog(context: Context, title: String,
                           msg: String, btnLabel1: String, btnLabel2: String,
                           method1: Method?, method2: Method?): Dialog {
        val str = SpannableString(title)
        str.setSpan(ForegroundColorSpan(Color.parseColor(green)), 0,
                title.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        val builder = AlertDialog.Builder(context)
        builder.setTitle(str)
        builder.setMessage(msg)
        builder.setPositiveButton(btnLabel1,
                object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface, which: Int) {

                        if (method1 != null)
                            method1!!.execute()
                    }
                })

        builder.setNegativeButton(btnLabel2,
                object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface, which: Int) {

                        if (method2 != null)
                            method2!!.execute()
                    }
                })

        val d = builder.show()
        val dividerId = d.context.resources
                .getIdentifier("android:id/titleDivider", null, null)
        val divider = d.findViewById<View>(dividerId)
        divider.setBackgroundColor(Color.parseColor(green))

        return d

    }

    fun showToastShort(context: Context, text: String) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
    }

    fun isEmailValid(email: String): Boolean {
        var isValid = false
        val expression = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"

        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(email)
        if (matcher.matches()) {
            isValid = true
        }
        return isValid
    }

    fun brandAlertDialog(dialog: AlertDialog) {
        try {
            val resources = dialog.context.resources
            // int color = resources.getColor(...); // your color here

            val titleDividerId = resources.getIdentifier("titleDivider", "id",
                    "android")
            val titleDivider = dialog.window!!.decorView
                    .findViewById<View>(titleDividerId)
            titleDivider.setBackgroundColor(Color.parseColor(green)) // change
            // divider
            // color
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun checkNetworkConnection(c: Context): Boolean {
        val connectivityManager = c
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager!!
                .activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo!!.isConnected
    }

    fun exitDialog(context: Context, msg: String,
                   method: Method?): Dialog {
        val chars = "Achai a Vaga"
        val str = SpannableString(chars)
        str.setSpan(ForegroundColorSpan(Color.parseColor(green)), 0,
                chars.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        val builder = AlertDialog.Builder(context)
        builder.setTitle(str)
        builder.setMessage(msg)
        builder.setPositiveButton(Constants.ok,
                object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface, which: Int) {
                        if (method != null)
                            method!!.execute()
                    }
                })

        builder.setNegativeButton(Constants.cancel,
                object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface, which: Int) {
                        dialog.dismiss()
                    }
                })
        val d = builder.show()
        val dividerId = d.context.resources
                .getIdentifier("android:id/titleDivider", null, null)
        val divider = d.findViewById<View>(dividerId)
        divider.setBackgroundColor(Color.parseColor(green))

        return d
        // return builder.create();
    }

    interface Method {
        fun execute()
    }

    fun write(message: String) {
        println(message)
    }

    fun sreenLock(activity: Activity) {
        val currentOrientation = activity.resources.configuration.orientation
        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE
        } else {
            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT
        }
    }

    fun hideSoftKeyboard(activity: Activity?) {
        try {
            if (activity != null) {
                val inputMethodManager = activity!!
                        .getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                val v = activity!!.currentFocus
                if (v != null) {
                    val binder = activity!!.currentFocus!!
                            .windowToken
                    if (binder != null) {
                        inputMethodManager!!.hideSoftInputFromWindow(binder, 0)
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    protected fun gotoActivity(activity: Activity,
                               classActivity: Class<*>, bundle: Bundle?) {
        //  val utils:Utils= Utils()
        utils.hideSoftKeyboard(activity)
        val intent = Intent(activity, classActivity)
        if (bundle != null)
            intent.putExtra("android.intent.extra.INTENT", bundle)
        activity.startActivity(intent)
    }

    fun setPreferenceFromNormalLogin(context: Context,
                                     forSharedPref: Bundle) {
        if (forSharedPref.size() > 0) {
            for (key in forSharedPref.keySet()) {
                val value = forSharedPref.get(key)
                setPrefrence(context, key, value as String)
            }
        }
    }

    fun setPrefrence(context: Context, key: String, value: String) {
        val prefrence = context.getSharedPreferences(
                "ParkingApp", 0)
        val editor = prefrence.edit()
        editor.putString(key, value)
        editor.commit()
    }

    /*
     * retreiving the data from shared preferences
     */
    fun getPrefrence(context: Context, key: String): String {
        val prefrence = context.getSharedPreferences(
                "ParkingApp", 0)
        return prefrence.getString(key, "")
    }
}