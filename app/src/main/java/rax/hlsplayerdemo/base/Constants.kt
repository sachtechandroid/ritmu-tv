package rax.hlsplayerdemo.base

import org.json.JSONArray
import java.util.ArrayList
import java.util.HashMap

object Constants {
    var noInternet = "No Internet Connection!"

    var server = "http://37.48.245.14/"
    var rootUrl = server + "webservice/index2.php/"
    var getLogInWithFacebook = rootUrl + "getLogInWithFacebook"
    var ok = "Ok"
    var cancel = "Cancel"

    var loginUrl = rootUrl + "getLogIn"
    var networkMsg = "Conex?o n?o dispon?vel Rede . Por favor, verifique sua conex?o de rede aned tente novamente."
   // var parse_obj: ParseUser? = null

   // var user = "_User"
    var username = "username"
    var password = "password"
    var FbKey = "FbKey"
    var name = "Name"
    var google = "google"
    var facebook = "facebook"
    var simple_email = "simpleEmail"
    var isActive = "IsActive"
    var isUser = "isUser"
    var Category = "Category"
    var PurchaseHistory = "PurchaseHistory"
//	public static String isLogin= "isLogin";

    //var markerMap = HashMap<Marker, MarkerDetails>()

    //	public static String address = "address";
    //	public static  ArrayList<JSONObject> parkingSpotArray;
    var currentClass = ""
//	public static boolean isPath=false;

    var isGooglePlus = false

//	public static ArrayList<JSONObject> arr= new ArrayList<JSONObject>();

    //	public static ArrayList<JSONObject> parkingStreet = new ArrayList<JSONObject>();
    var parkingHistory = ArrayList<JSONArray>()
    var markerClickIndex = -1
    var previousPinIndex = 0
    var drawerListIndex = 0


    var isAvailed = false

    var Street = "Street"
    var StreetObjectId = "objectId"
    var StreetName = "StreetName"
    var City = "City"
    var Neighborhood = "Neighborhood"
    var StreetisActive = "isActive"
    var ParkingType = "ParkingType"

    var ParkingSpot = "ParkingSpot"
    var Latitude = "Latitude"
    var ParkingSpotObjectId = "objectId"
    var Longitude = "Longitude"
    var StreetID = "StreetID"
    var BikerID = "BikerID"
    var IsVisible = "IsVisible"
    var IsAvailable = "IsAvailable"
    var ParkingSpotNumber = "ParkingSpotNumber"
    var IsCategory_Senior = "IsCategory_Senior"
    var IsCategory_Disabled = "IsCategory_Disabled"
    var IsCategory_Cyclist = "IsCategory_Cyclist"
    var IsCategory_Motorcyclist = "IsCategory_Motorcyclist"
    var IsCategory_Truck = "IsCategory_Truck"
    var ParkingSpotID = "ParkingSpotID"

}