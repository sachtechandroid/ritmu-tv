package rax.hlsplayerdemo.ui

import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import kotlinx.android.synthetic.main.activity_home_page.*
import rax.hlsplayerdemo.R
import rax.hlsplayerdemo.base.KotlinBaseActivity
import rax.hlsplayerdemo.extension.toast

class HomePageActivity : KotlinBaseActivity(container = R.id.container), View.OnClickListener {


    var container:FrameLayout?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_page)
        container=findViewById(R.id.container)

        rtv.setOnClickListener(this)
        refresh.setOnClickListener(this)
        home.setOnClickListener(this)
        addFragment(HomeFragment::class,tag = HomeFragment::class.simpleName!!)
    }

    override fun onClick(v: View?) {
        when(v!!.id)
        {
            R.id.home->
            {
                if (index==1)
                    toast("Home Page")
                else
                    navigateToFragment(HomeFragment::class)
            }
            R.id.rtv->
            {
                if (index==2)
                    toast("Live Tv")
                else
                    navigateToFragment(LiveTvFragment::class)
            }
            R.id.refresh->
            {
                when (index) {
                    1 -> navigateToFragment(HomeFragment::class)
                    2 -> {
                        refreshFragment(LiveTvFragment::class.simpleName!!)

                    }
                    else -> refreshFragment(SelectionFragment::class.simpleName!!)
                }
            }
        }
    }
    companion object {
        var index=0
    }
}
