package rax.hlsplayerdemo.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.res.Configuration
import android.graphics.Color
import android.media.MediaPlayer
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.VideoView
import com.google.firebase.database.*
import rax.hlsplayerdemo.R
import rax.hlsplayerdemo.base.KotlinBaseFragment
import rax.hlsplayerdemo.extension.executeSafe
import rax.hlsplayerdemo.extension.toast
import rax.hlsplayerdemo.extension.workerRepeatRH
import rax.hlsplayerdemo.widget.ScrollViewText
import java.net.HttpURLConnection
import java.net.URL
import java.util.*


class LiveTvFragment : KotlinBaseFragment(R.layout.fragment_livetv), MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener {

    private var urlStream: String? = null
    private val descriptiona: String? = null
    private var myVideoView: VideoView? = null
    internal var Pdialog: ProgressDialog? = null
    internal var bar: ProgressBar? = null
    internal var timeTexta: TextView? = null
    internal var descriptionText: ScrollViewText? = null
    internal var descUpText: String? = null
    internal var runnable: Runnable? = null
    internal var database: FirebaseDatabase? = null
    internal var myRef: DatabaseReference? = null
    internal var lastOrientation = 0
    var timerHandler: Handler? = null
    internal var handler: Handler? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /*activity!!.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        activity!!.window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        activity!!.window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)*/
        setTime(view)
        descriptionText = view.findViewById(R.id.descriptionText)
        descriptionText!!.startScroll()

        /*val settings = descriptionText!!.settings
        settings.javaScriptEnabled = true
        settings.domStorageEnabled = true
        descriptionText!!.isSelected = true
        descriptionText!!.isEnabled = true*/
        loadDescriptionOnWebView("Ritmu TV")
        bar = view.findViewById<View>(R.id.test) as ProgressBar
        Pdialog = ProgressDialog(context)
        Pdialog!!.setMessage("Connecting...")
        Pdialog!!.show()
        bar!!.visibility = View.GONE

        database = FirebaseDatabase.getInstance()
        myRef = database!!.getReference("message")
        getDescriptionUpdated()
        getDescriptionMessage()
        myVideoView = view.findViewById<View>(R.id.myVideoView) as VideoView
        myVideoView!!.setOnPreparedListener(this)
        with(myVideoView!!) {
            myVideoView!!.requestFocus()
            setZOrderOnTop(true)
            setBackgroundColor(Color.TRANSPARENT)
        }
        myVideoView!!.setOnErrorListener(this)
        urlStream = "rtsp://ritmu.flashmediacast.com:1935/ritmu/livestream"
        /*new getDescriptionMessage().execute();*/
        activity!!.runOnUiThread {
            myVideoView!!.setVideoURI(Uri.parse(urlStream))
            myVideoView!!.start()
        }
    }

    private fun getDescriptionUpdated() {
        workerRepeatRH(30000) {
            executeSafe {
                handler!!.postDelayed(runnable, 30000)
                GetDescriptionApi().execute()
            }
        }
    }

    private fun getDescriptionMessage() {
        myRef!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val descMessage = dataSnapshot.getValue(String::class.java)
                loadDescriptionOnWebView(descMessage)
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Log.e("sa", "")
            }
        })

    }

    private fun setTime(view: View) {
        timeTexta = view.findViewById(R.id.timeTextab)

        timerHandler = context?.workerRepeatRH(1000) {
            val cal = GregorianCalendar(TimeZone.getTimeZone("Asia/Kuwait"))

            val hour12 = cal.get(Calendar.HOUR)
            val minutes = cal.get(Calendar.MINUTE)
            val seconds = cal.get(Calendar.SECOND)
            val am = cal.get(Calendar.AM_PM) == Calendar.AM

            if (hour12.toString().length == 1) {
                if (am) {
                    timeTexta!!.text = "0$hour12:$minutes:$seconds AM"
                } else {
                    timeTexta!!.text = "0$hour12:$minutes:$seconds PM"
                }
            } else if (minutes.toString().length == 1) {
                if (am) {
                    timeTexta!!.text = "$hour12:0$minutes:$seconds AM"
                } else {
                    timeTexta!!.text = "$hour12:0$minutes:$seconds PM"
                }
            } else if (seconds.toString().length == 1) {
                if (am) {
                    timeTexta!!.text = "$hour12:$minutes:0$seconds AM"
                } else {
                    timeTexta!!.text = "$hour12:$minutes:0$seconds PM"
                }
            } else {
                if (am) {
                    timeTexta!!.text = "$hour12:$minutes:$seconds AM"
                } else {
                    timeTexta!!.text = "$hour12:$minutes:$seconds PM"
                }
            }
        }

    }


    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (lastOrientation != newConfig.orientation) {
            lastOrientation = newConfig.orientation
        }
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            val params = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
            params.addRule(RelativeLayout.ABOVE, R.id.timeTextab)
            params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE)
            params.addRule(RelativeLayout.ALIGN_PARENT_START, RelativeLayout.TRUE)
            params.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE)
            myVideoView?.layoutParams = params
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            val params = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
            params.removeRule(RelativeLayout.ABOVE)
            params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE)
            params.addRule(RelativeLayout.ALIGN_PARENT_START, RelativeLayout.TRUE)
            params.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE)
            myVideoView?.layoutParams = params
        }
    }

    override fun onPrepared(mediaPlayer: MediaPlayer) {
        mediaPlayer.setOnInfoListener(MediaPlayer.OnInfoListener { mp, what, extra ->
            if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                if (bar!!.isActivated) {
                    bar!!.visibility = View.GONE
                }
                if (Pdialog!!.isShowing) {
                    Pdialog!!.dismiss()
                }
                return@OnInfoListener true
            }
            true
        })
    }

    override fun onDestroyOptionsMenu() {
        super.onDestroyOptionsMenu()
        myVideoView!!.stopPlayback()
    }

    override fun onResume() {
        super.onResume()
        HomePageActivity.index = 2
        toast("Resuming PlayBack")
    }


    override fun onPause() {
        super.onPause()
        myVideoView!!.suspend()


    }

    override fun onError(mp: MediaPlayer, what: Int, extra: Int): Boolean {
        if (Pdialog!!.isShowing) {
            Pdialog!!.dismiss()
        }
        if (bar!!.isEnabled) {
            bar!!.visibility = View.GONE
        }
        return true
    }

    @SuppressLint("StaticFieldLeak")
    private inner class GetDescriptionApi : AsyncTask<Void, Void, String>() {
        override fun doInBackground(vararg params: Void): String? {
            try {
                val url = URL("http://ritmu.tv/Ritmu/admin/update_data.php")
                val connection = url.openConnection() as HttpURLConnection
                connection.setRequestProperty("USER-AGENT", "Mozilla/5.0")
                connection.setRequestProperty("ACCEPT-LANGUAGE", "en-US,en;0.5")
                connection.doOutput = true
                connection.inputStream
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return null
        }
    }


    override fun onDestroyView() {
        timerHandler?.removeCallbacksAndMessages(null)
        timerHandler = null
        super.onDestroyView()
    }

    fun loadDescriptionOnWebView(description: String?) {
        descriptionText?.text = description
        /*descriptionText!!.loadUrl("about:blank")
        val summary = ("<html><body style=\"background:#465A61\"><font color=\"WHITE\" style=\"font-size:22px;font-weight:bold;line-height:25px;\"/ ><marquee behaviour=slide>"
                + description
                + "</marquee></font></body></html>")
        descriptionText!!.loadData(summary, "text/html", "utf-8")
        descriptionText!!.visibility = View.VISIBLE*/
    }


}
