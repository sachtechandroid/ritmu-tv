package rax.hlsplayerdemo.ui

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import rax.hlsplayerdemo.R
import rax.hlsplayerdemo.base.KotlinBaseActivity
import rax.hlsplayerdemo.extension.isInternet

class SplashActivity : KotlinBaseActivity(), Animation.AnimationListener {
    internal var fadein: Animation?=null
    internal var applogo: ImageView?=null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        setContentView(R.layout.activity_splash)
        applogo = findViewById<View>(R.id.applogo) as ImageView
        fadein = AnimationUtils.loadAnimation(applicationContext, R.anim.fade_in)
        fadein!!.setAnimationListener(this)
        applogo!!.animation = fadein
    }

    override fun onAnimationStart(animation: Animation) {

    }

    override fun onAnimationEnd(animation: Animation) {
        if (!isInternet()) {
            val builder = AlertDialog.Builder(this@SplashActivity)

            builder.setTitle("Connection Error")
            builder.setCancelable(false)
            builder.setMessage("Please Check Your Internet Connection")
            val dialog = builder.create()
            dialog.window!!.setDimAmount(0.7f)
            dialog.show()
        } else {
            openActivity(HomePageActivity::class)
            finish()
        }
    }

    override fun onAnimationRepeat(animation: Animation) {

    }
}
