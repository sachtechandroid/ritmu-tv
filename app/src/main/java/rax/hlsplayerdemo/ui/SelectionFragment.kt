package rax.hlsplayerdemo.ui

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.fragment_selection.*
import rax.hlsplayerdemo.R
import rax.hlsplayerdemo.base.KotlinBaseFragment
import rax.hlsplayerdemo.extension.gone
import rax.hlsplayerdemo.extension.isInternet
import rax.hlsplayerdemo.extension.toast
import rax.hlsplayerdemo.extension.visible
import rax.hlsplayerdemo.listeners.Api_Hitter
import rax.hlsplayerdemo.listeners.draw_bean.DrawDataItem
import rax.hlsplayerdemo.listeners.draw_bean.DrawResponse
import rax.hlsplayerdemo.listeners.draw_bean.ErrorModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*


class SelectionFragment : KotlinBaseFragment(R.layout.fragment_selection) {
    var SelectedDay = ""
    var SelectedMonth = ""
    var SelectedYear = ""
    var date: Calendar? = null
    var progressDialog: ProgressDialog? = null
    val adapter by lazy { DrawListAdapter(context!!) }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        calendar.setOnClickListener { DateDialog() }
        txtShow.setOnClickListener { getData() }
        progressDialog = ProgressDialog(context)
        progressDialog!!.setMessage("Wait a sec...")
        progressDialog!!.setCanceledOnTouchOutside(false)
        /*progressDialog!!.setCancelable(tr)*/
        recyclerView.adapter = adapter
        adapter.clear()
        date = Calendar.getInstance()
        adapter.notifyDataSetChanged()
        SelectedYear = date!!.get(Calendar.YEAR).toString()
        var month = date!!.get(Calendar.MONTH) + 1
        SelectedMonth = month.toString()
        SelectedDay = date!!.get(Calendar.DAY_OF_MONTH).toString()
        txtDate.text = "$SelectedYear-$SelectedMonth-$SelectedDay"
        getDrawList(txtDate.text.toString())

    }

    private fun getData() {
        if (!isInternet())
            toast("enable internet")
        else {
            getDrawList(txtDate.text.toString())
        }
    }

    fun DateDialog() {

        val listener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            var month=monthOfYear+1
            txtDate.text = year.toString()+"-"+month+"-"+dayOfMonth.toString()
            //SelectedDate=year.toString()+"-"+month.toString()+"-"+dayOfMonth.toString()
        }
        val dpDialog = DatePickerDialog(activity!!, listener, date!!.get(Calendar.YEAR), date!!.get(Calendar.MONTH), date!!.get(Calendar.DAY_OF_MONTH))
        dpDialog.show()
    }

    override fun onResume() {
        super.onResume()
        HomePageActivity.index = 3

    }

    fun getDrawList(selectedDate: String) {
        progressDialog!!.show()
        val gson = Gson()
        var getRegisterCall = Api_Hitter.ApiInterface().getDrawList(selectedDate, "getDrawValue")

        getRegisterCall?.enqueue(object : Callback<JsonObject> {

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                progressDialog!!.hide()
                toast(t.message!!)
            }

            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                if (response.body() == null && response.errorBody() != null) {

                    val errorConverter = Api_Hitter.retrofit!!.responseBodyConverter<ErrorModel>(ErrorModel::class.java, arrayOfNulls(0))
                    try {
                        val error = errorConverter.convert(response.errorBody()!!)

                        Toast.makeText(activity, error.message, Toast.LENGTH_SHORT).show()
                    } catch (e: IOException) {
                        progressDialog!!.hide()
                        toast(e.message.toString())
                        e.printStackTrace()

                    }

                } else if (response.body() != null) {

                    if (response.body()!!.asJsonObject.get("Status").asString.equals("Failure")) {
                        adapter.clear()
                        adapter.notifyDataSetChanged()
                        progressDialog!!.hide()
                        layout_draw.gone()
                        txtEmpty.visible()
                        txtEmpty.text = response.body()!!.asJsonObject.get("Message").asString.toString()
                    } else {
                        progressDialog!!.hide()
                        txtEmpty.gone()
                        layout_draw.visible()
                        val data = gson.fromJson(response.body(), DrawResponse::class.java)
                        txtEmpty.text = data.drawData!![0]!!.date.toString()
                        adapter.clear()
                        adapter.add(data.drawData as ArrayList<DrawDataItem>)
                        adapter.notifyDataSetChanged()

                    }

                }

            }

        })
    }
}