package rax.hlsplayerdemo.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.res.Configuration
import android.media.MediaPlayer
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.webkit.WebView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import android.widget.VideoView
import com.google.firebase.database.*
import rax.hlsplayerdemo.R
import rax.hlsplayerdemo.base.KotlinBaseActivity
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class Hls_player : KotlinBaseActivity(), MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener {

    private var urlStream: String? = null
    private val descriptiona: String? = null
    private var myVideoView: VideoView? = null
    internal var Pdialog: ProgressDialog? = null
    internal var bar: ProgressBar? = null
    internal var timeTexta: TextView? = null
    internal var descriptionWebview: WebView? = null
    internal var descUpText: String? = null
    internal var runnable: Runnable? = null
    internal var database: FirebaseDatabase? = null
    internal var myRef: DatabaseReference? = null
    internal var lastOrientation = 0
    internal var handler: Handler? = null

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        setContentView(R.layout.activity_hls_player)
        setTime()
        descriptionWebview = findViewById<View>(R.id.descriptionText) as WebView
        val settings = descriptionWebview!!.settings
        settings.javaScriptEnabled = true
        descriptionWebview!!.isSelected = true
        descriptionWebview!!.isEnabled = true
        loadDescriptionOnWeView("Ritmu TV")
        bar = findViewById<View>(R.id.test) as ProgressBar
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            Pdialog = ProgressDialog(this)
            Pdialog!!.setMessage("Connecting...")
            Pdialog!!.show()
            bar!!.visibility = View.GONE
        }
        database = FirebaseDatabase.getInstance()
        myRef = database!!.getReference("message")
        getDescriptionMessage()
        myVideoView = findViewById<View>(R.id.myVideoView) as VideoView
        myVideoView!!.requestFocus()
        //myVideoView.setZOrderOnTop(true);
        myVideoView!!.setOnPreparedListener(this)
        myVideoView!!.setOnErrorListener(this)
        urlStream = "rtsp://ritmu.flashmediacast.com:1935/ritmu/livestream"
        /*new getDescriptionMessage().execute();*/
        getDescriptionUpdated()
        runOnUiThread {
            myVideoView!!.setVideoURI(Uri.parse(urlStream))
            myVideoView!!.start()
        }
    }

    private fun getDescriptionUpdated() {
        handler = Handler()
        runnable = Runnable {
            try {
                handler!!.postDelayed(runnable, 30000)
                GetDescriptionApi().execute()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        handler!!.postDelayed(runnable, 30000)
    }

    private fun getDescriptionMessage() {
        myRef!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val desscriptionMeassage = dataSnapshot.getValue(String::class.java)
                loadDescriptionOnWeView(desscriptionMeassage)
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    private fun setTime() {
        timeTexta = findViewById(R.id.timeTextab)
        val t = object : Thread() {

            override fun run() {
                try {
                    while (!isInterrupted) {
                        Thread.sleep(1000)
                        runOnUiThread {
                            val cal = GregorianCalendar(TimeZone.getTimeZone("Asia/Kuwait"))

                            val hour12 = cal.get(Calendar.HOUR)
                            val minutes = cal.get(Calendar.MINUTE)
                            val seconds = cal.get(Calendar.SECOND)
                            val am = cal.get(Calendar.AM_PM) == Calendar.AM

                            if (hour12.toString().length == 1) {
                                if (am) {
                                    timeTexta!!.text = "0$hour12:$minutes:$seconds AM"
                                } else {
                                    timeTexta!!.text = "0$hour12:$minutes:$seconds PM"
                                }
                            } else if (minutes.toString().length == 1) {
                                if (am) {
                                    timeTexta!!.text = "$hour12:0$minutes:$seconds AM"
                                } else {
                                    timeTexta!!.text = "$hour12:0$minutes:$seconds PM"
                                }
                            } else if (seconds.toString().length == 1) {
                                if (am) {
                                    timeTexta!!.text = "$hour12:$minutes:0$seconds AM"
                                } else {
                                    timeTexta!!.text = "$hour12:$minutes:0$seconds PM"
                                }
                            } else {
                                if (am) {
                                    timeTexta!!.text = "$hour12:$minutes:$seconds AM"
                                } else {
                                    timeTexta!!.text = "$hour12:$minutes:$seconds PM"
                                }
                            }
                        }
                    }
                } catch (e: InterruptedException) {
                }

            }
        }

        t.start()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (lastOrientation != newConfig.orientation) {
            lastOrientation = newConfig.orientation
        }
    }

    override fun onPrepared(mediaPlayer: MediaPlayer) {
        mediaPlayer.setOnInfoListener(MediaPlayer.OnInfoListener { mp, what, extra ->
            if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                if (bar!!.isActivated) {
                    bar!!.visibility = View.GONE
                }
                if (Pdialog!!.isShowing) {
                    Pdialog!!.dismiss()
                }
                return@OnInfoListener true
            }
            true
        })
    }

    override fun onResume() {
        super.onResume()
        Toast.makeText(this, "Resuming PlayBack", Toast.LENGTH_SHORT).show()
    }

    override fun onPause() {
        super.onPause()
        myVideoView!!.suspend()
    }

    override fun onError(mp: MediaPlayer, what: Int, extra: Int): Boolean {
        if (Pdialog!!.isShowing) {
            Pdialog!!.dismiss()
        }
        if (bar!!.isEnabled) {
            bar!!.visibility = View.GONE
        }
        return true
    }

    private inner class GetDescriptionApi : AsyncTask<Void, Void, String>() {
        override fun doInBackground(vararg params: Void): String? {
            try {
                val url = URL("http://ritmu.tv/Ritmu/admin/update_data.php")
                val connection = url.openConnection() as HttpURLConnection
                connection.setRequestProperty("USER-AGENT", "Mozilla/5.0")
                connection.setRequestProperty("ACCEPT-LANGUAGE", "en-US,en;0.5")
                connection.doOutput = true
                connection.inputStream
                Log.e("fuck", connection.inputStream.toString() + "")
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return null
        }
    }

    /*private class getDescriptionMessage extends AsyncTask<Void, Void, String> {

    @Override protected String doInBackground(Void... params) {
      try {
        URL url = new URL("http://ritmu.tv/Ritmu/admin/api/videoProcess.php");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        String urlParameters = "type=getDescriptionMessage";
        connection.setRequestMethod("POST");
        connection.setRequestProperty("USER-AGENT", "Mozilla/5.0");
        connection.setRequestProperty("ACCEPT-LANGUAGE", "en-US,en;0.5");
        connection.setDoOutput(true);
        DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
        dStream.writeBytes(urlParameters);
        dStream.flush();
        dStream.close();
        int responseCode = connection.getResponseCode();
        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line = "";
        StringBuilder responseOutput = new StringBuilder();
        while ((line = br.readLine()) != null) {
          responseOutput.append(line);
        }
        br.close();
        try {
          JSONObject desc = new JSONObject(responseOutput.toString());
          JSONArray data = desc.getJSONArray("data");
          for (int i = 0; i < data.length(); i++) {
            JSONObject jsonObject = data.getJSONObject(i);
            descriptiona = jsonObject.getString("message").toString();
          }
        } catch (JSONException e) {
          e.printStackTrace();
          loadDescriptionOnWebView("RITMU TV");
        }
        Hls_player.this.runOnUiThread(new Runnable() {

          @Override public void run() {
            loadDescriptionOnWebView(descriptiona);
          }
        });
      } catch (MalformedURLException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      }
      return null;
    }
  }
*/
    fun loadDescriptionOnWeView(description: String?) {
        descriptionWebview!!.loadUrl("about:blank")
        val summary = ("<html><body style=\"background:#304d79\"><font color=\"WHITE\" style=\"font-size:22px;font-weight:bold;line-height:33px;\"/ ><marquee behaviour=slide>"
                + description
                + "</marquee></font></body></html>")
        descriptionWebview!!.loadData(summary, "text/html", "utf-8")
        descriptionWebview!!.visibility = View.VISIBLE
    }
}

