package rax.hlsplayerdemo.ui

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.view_drawlist.view.*
import rax.hlsplayerdemo.R
import rax.hlsplayerdemo.listeners.draw_bean.DrawDataItem


class DrawListAdapter(val context: Context) : RecyclerView.Adapter<DrawListAdapter.MyViewHolder>() {


        var data=ArrayList<DrawDataItem>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val itemView: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.view_drawlist, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var data= data[position]
        holder.itemView.draw_time.text = data!!.time.toString()
        holder.itemView.draw.text = data.draw.toString()

    }

    override fun getItemCount(): Int {
        return data!!.size
    }

    fun add(dataItem: ArrayList<DrawDataItem>)
    {
        data.addAll(dataItem)
        notifyDataSetChanged()
    }

    fun clear() {

        data.clear()
        notifyDataSetChanged()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)
}

