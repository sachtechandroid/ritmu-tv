package rax.hlsplayerdemo.ui

import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_home.*
import rax.hlsplayerdemo.R
import rax.hlsplayerdemo.base.KotlinBaseFragment
import rax.hlsplayerdemo.extension.isInternet
import rax.hlsplayerdemo.extension.toast

class HomeFragment:KotlinBaseFragment(R.layout.fragment_home), View.OnClickListener {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //listner
        live_tv.setOnClickListener(this)
        draw_tv.setOnClickListener(this)
    }

    override fun onClick(v: View?) {

        when(v!!.id)
        {
            R.id.live_tv->
            {
                if (!isInternet())
                toast("enable internet")
                else
                baselistener.navigateToFragment(LiveTvFragment::class)
            }
            R.id.draw_tv->{
                if (!isInternet())
                    toast("enable internet")
                else
                baselistener.navigateToFragment(SelectionFragment::class)
            }
        }
    }
fun id (): Int {
    return this.id
}

    override fun onResume() {
        super.onResume()
        HomePageActivity.index=1
    }
}